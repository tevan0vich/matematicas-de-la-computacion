#include <stdio.h>

using namespace std;

int GCD(int, int);
int MCM(int, int);

int main() {
	int a,b;
	scanf("%d",&a);
	scanf("%d",&b);
	if(b<a) {
		int aux=b;
		b=a;
		a=aux;
	}
	int res=GCD(a,b);
	printf("%d",res);
	return 0;
}
int MCM(int a, int b){
	return((a*b)/GCD(a,b));
}
int GCD(int a,int b) {
	if(b==0)
		return a;
	return GCD(b,a%b);
}
