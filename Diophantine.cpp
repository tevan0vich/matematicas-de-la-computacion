#include <stdio.h>
#include <stdlib.h>

long a,b,d,x,y;

void EE(long a,long b,long *d,long *x,long *y);

int main(){
	int test;
	scanf("%ld%ld%d",&a,&b,&test);
	EE(a,b,&d,&x,&y);
	if(test%d==0){
		x*=(test/d);
		y*=(test/d);
		printf("%ld(%ld) + %ld(%ld) = %d",a,x,b,y,test);
	}
	else
		printf("No Solution");
	return 0;
}
void EE(long a,long b,long *d,long *x,long *y){
	long xl,yl;
	if(b==0){
		*d = a;
		*x = 1;
		*y = 0;
	}else{
		EE(b,a%b,d,x,y);
		xl = *x;
		yl = *y;
		*x = yl;
		*y=xl-(a/b)*yl;
	}
}

