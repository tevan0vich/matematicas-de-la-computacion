#include <stdio.h>

using namespace std;

float combinacionesUP(int, int);

int main() {
	int n, k;
	scanf("%d%d", &n, &k);
	printf("%.0f", combinacionesUP(n, k));
	return 0;
}
float combinacionesUP(int n, int k) {
	float res = 1;
	float temp;
	for (float i = 1, j = n; i <= k; i++, j--) {
		temp = (j / i);
		res *= temp;
	}
	return res;
}
