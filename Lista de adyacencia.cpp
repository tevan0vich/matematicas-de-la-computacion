#include <stdio.h>
#include <list>
#include <vector>

using namespace std;

vector<list <int> > M;

int main() {
	int n,m;
	list <int>::iterator it;
	scanf("%d%d",&n,&m);
	M.resize(n);
	for(int i=0;i<m;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		M[a].push_back(b);
		M[b].push_back(a);
	}
	for(int i=0;i<M.size();i++){
		printf("%d: ",i);
		for(it=M[i].begin();it!=M[i].end();it++){
			if(it!=M[i].begin())
				printf("->");
			printf("%d",*it);
		}
		printf("\n");
	}
}
