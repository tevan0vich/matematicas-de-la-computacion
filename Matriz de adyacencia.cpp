#include <stdio.h>

using namespace std;

int main() {
	int n=4,m=4;
	scanf("%d%d",&n,&m);
	int M[n][m];
	
	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			M[i][j]=0;
	
	for(int i=0; i<m; i++) {
		int x,y;
		scanf("%d%d",&x,&y);
		M[x][y]=1;
		M[y][x]=1;
	}
	for(int i=0; i<n; i++) {
		for(int j=0; j<n; j++) {
			printf("%d ",M[i][j]);
		}
		printf("\n");
	}
}
